﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LV3
{
    class Dataset:Prototype
    {
        private List<List<string>> data; 
        public Dataset()
        {
            this.data = new List<List<string>>();
        }
        public Dataset(string filePath) : this()
        {
            this.LoadDataFromCSV(filePath);
        }
        public Dataset(Dataset reference)
        {
            data = new List<List<string>>();
            for (int i = 0; i < reference.data.Count; i++)
            {
                data.Add(new List<string>(reference.data[i]));

            }

        }
        public void LoadDataFromCSV(string filePath)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    List<string> row = new List<string>();
                    string[] items = line.Split(',');
                    foreach (string item in items)
                    {
                        row.Add(item);
                    }
                    this.data.Add(row);
                }
            }
        }
        public IList<List<string>> GetData()
        {
            return
           new System.Collections.ObjectModel.ReadOnlyCollection<List<string>>(data);
        }
        public void ClearData()
        {
            this.data.Clear();
        }
        public Prototype Clone()
        {
            return (Prototype)new Dataset(this);
        }
    }
}
