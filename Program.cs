﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LV3
{
    class Program
    {
        static void Main(string[] args)
        {
            //potrebno nam je duboko kopiranje zbog toga što je atribut data reference tipa i plitko kopitanje će nam dati kopiju koja će pokazivati na istu tu instancu

            Dataset fileData = new Dataset("csv.txt");
            IList<List<string>> readonlydata = fileData.GetData();
            foreach (List<string> row in readonlydata)
            {
                foreach (string data in row)
                {
                    Console.WriteLine(data);
                }
            }

            Dataset fileData2 = (Dataset)fileData.Clone();
            IList<List<string>> readonlydata2 = fileData2.GetData();
            foreach (List<string> row in readonlydata2)
            {
                foreach (string data in row)
                {
                    Console.WriteLine(data);
                }
            }
            fileData.ClearData();
            readonlydata2 = fileData2.GetData();
            Console.WriteLine("Provjera fileData2 nakon kopiranja: ");
            foreach (List<string> row in readonlydata2)
            {
                foreach (string data in row)
                {
                    Console.WriteLine(data);
                }
            }
            System.Console.Read();
            // DRUGI ZADATAK
            MatrixGenerator generator = MatrixGenerator.GetInstance();
            double[][] matrix = generator.NextMatrix(3, 4);
            foreach (double[] row in matrix)
            {
                foreach (double element in row)
                {
                    Console.Write(element + " ");
                }
                Console.WriteLine();
            }
            System.Console.Read(); 
        }
    }
}
