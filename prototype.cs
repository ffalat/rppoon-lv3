﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LV3
{
    interface Prototype
    {
        Prototype Clone();
    }
}
